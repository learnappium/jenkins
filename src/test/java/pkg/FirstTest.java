package pkg;

import junit.framework.Assert;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class FirstTest {
	
	public static WebDriver driver;
	
	
	@BeforeTest
	public void setup() throws InterruptedException{
		
		driver = new FirefoxDriver();
		driver.navigate().to("http://www.events.com");
		Thread.sleep(5000L);
	}
	
	@Test
	public void test(){
		
		
		Assert.assertEquals("Events", driver.getTitle());	
		System.out.println("test is running");
	}

	
	@AfterTest
	public void quit(){
		driver.quit();
	}
}
